<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsuariospagosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsuariospagosTable Test Case
 */
class UsuariospagosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UsuariospagosTable
     */
    public $Usuariospagos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.usuariospagos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Usuariospagos') ? [] : ['className' => UsuariospagosTable::class];
        $this->Usuariospagos = TableRegistry::get('Usuariospagos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Usuariospagos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
