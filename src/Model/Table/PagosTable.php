<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\Datasource\EntityInterface;
use ArrayObject;
/**
 * Pagos Model
 *
 * @method \App\Model\Entity\Pago get($primaryKey, $options = [])
 * @method \App\Model\Entity\Pago newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Pago[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Pago|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Pago patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Pago[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Pago findOrCreate($search, callable $callback = null, $options = [])
 */
class PagosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('pagos');
        $this->setDisplayField('codigopago');
        $this->setPrimaryKey('codigopago');

        $this->hasOne('Usuariospagos',[
            'foreignKey' => 'codigopago'
        ])->setDependent(true);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('codigopago')
            ->allowEmpty('codigopago', 'create');

        $validator
            ->decimal('importe',null,"Este campo debe ser numerico")
            ->requirePresence('importe', true,'El campo es requerido')
            ->notEmpty('importe','El importe no puede estar vacio')
            ->greaterThan("importe",0,"el importe no puede ser menor o igual a cero");

        $validator
            ->date('fecha',["ymd"],"la fecha es invalida, el formato es: Y-m-d")
            ->requirePresence('fecha', true,'El campo es requerido')
            ->notEmpty('fecha','la fecha no puede estar vacia')
            ->add('fecha', [
                'fechaMayor' => ['rule' => 'fechaMayor', 'message' => 'La fecha no puede ser menor a la actual','provider'=>'table']
            ]);


        return $validator;
    }

    function fechaMayor($value){
            if(is_array($value)){
                $value = implode("-",$value);
            }
        return strtotime(date("Y-m-d",strtotime($value))) > strtotime("-1 days") ;
    }
}
