<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Usuariospagos Model
 *
 * @method \App\Model\Entity\Usuariospago get($primaryKey, $options = [])
 * @method \App\Model\Entity\Usuariospago newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Usuariospago[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Usuariospago|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Usuariospago patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Usuariospago[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Usuariospago findOrCreate($search, callable $callback = null, $options = [])
 */
class UsuariospagosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->setPrimaryKey('id');
        $this->setTable('usuariospagos');

        $this->belongsTo("Usuarios",[
            "foreignKey" => 'codigousuario'
        ]);
        $this->belongsTo("Pagos",[
            "foreignKey" => 'codigopago'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('codigopago')
            ->requirePresence('codigopago', true,'El campo es requerido')
            ->notEmpty('codigopago','el codigo del pago no puede estar vacio');

        $validator
            ->integer('codigousuario')
            ->requirePresence('codigousuario', true,'El campo es requerido')
            ->notEmpty('codigousuario','el codigo del usuario no puede estar vacio');

        $validator->add('codigousuario', [
            'checkCustomerExists' => ['rule' => 'checkUsuarioExists', 'message' => 'Este usuario no existe','provider'=>'table']
        ]);
        $validator->add('codigopago', [
            'checkPagoExists' => ['rule' => 'checkPagoExists', 'message' => 'Este pago no existe','provider'=>'table']
        ]);
        return $validator;
    }

    public function checkUsuarioExists($value,$context){
        return $this->Usuarios->exists(["Usuarios.codigousuario" => $value]);
    }

    public function checkPagoExists($value,$context){
        return $this->Pagos->exists(["Pagos.codigopago" => $value]);
    }

    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['codigopago','codigousuario'],"Este usuario ya tiene este pago asigando"));
        return $rules;
    }
}
