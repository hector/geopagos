<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Favoritos Model
 *
 * @method \App\Model\Entity\Favorito get($primaryKey, $options = [])
 * @method \App\Model\Entity\Favorito newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Favorito[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Favorito|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Favorito patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Favorito[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Favorito findOrCreate($search, callable $callback = null, $options = [])
 */
class FavoritosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('favoritos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');


        $this->hasMany('Usuarios',[
            'foreignKey' => 'codigousuariofavorito',
            'cascadeCallbacks' => true
        ])->setDependent(true);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('codigousuario')
            ->requirePresence('codigousuario', 'create')
            ->notEmpty('codigousuario');

        $validator
            ->integer('codigousuariofavorito')
            ->requirePresence('codigousuariofavorito', 'create')
            ->notEmpty('codigousuariofavorito');


        $validator->add('codigousuario', [
              'checkCustomerExists' => ['rule' => 'checkUsuarioExists', 'message' => 'Este usuario no existe','provider'=>'table']
        ]);
        $validator->add('codigousuariofavorito', [
            'checkCustomerExists' => ['rule' => 'checkUsuarioExists', 'message' => 'Este usuario no existe','provider'=>'table']
        ]);



        return $validator;
    }

    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['codigousuariofavorito','codigousuario'],"Este usuario ya tiene este favorito asigando"));
        return $rules;
    }

    public function checkUsuarioExists($value,$context){
        return $this->Usuarios->exists(["Usuarios.codigousuario" => $value]);
    }
}
