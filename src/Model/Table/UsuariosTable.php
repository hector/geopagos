<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\Datasource\EntityInterface;
use ArrayObject;
/**
 * Usuarios Model
 *
 * @method \App\Model\Entity\Usuario get($primaryKey, $options = [])
 * @method \App\Model\Entity\Usuario newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Usuario[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Usuario|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Usuario patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Usuario[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Usuario findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsuariosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('usuarios');
        $this->setDisplayField('codigousuario');
        $this->setPrimaryKey('codigousuario');

        $this->addBehavior('Timestamp');


        $this->hasMany('Favoritos',[
                'foreignKey' => 'codigousuario'
            ])->setDependent(true);

        $this->hasMany('Usuariospagos',[
            'foreignKey' => 'codigousuario'
        ])->setDependent(true);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('codigousuario')
            ->allowEmpty('codigousuario', 'create');



        $validator
            ->scalar('usuario')
            ->requirePresence('usuario', 'create','El campo es requerido')
            ->notEmpty('usuario','El usuario no puede estar vacio');


        $validator
            ->requirePresence('clave','create','This field is required')
            ->notEmpty('clave','La clave no puede estar vacia','create')
            ->add('clave', 'complexity', ['rule' => ['containsNonAlphaNumeric', 2],'message' => 'La clave debe tener al menos 2 caracteres especiales'])
            ->add('clave', [
                'size' => ['rule' => ['minLength', 6],'message' => 'La clave debe tener al menos 6 caracteres']

            ]);


        $validator
            ->integer('edad','la edad debe ser un numero')
            ->requirePresence('usuario', 'create','El la edad es requerida')
            ->greaterThan("edad",17,"La edad no puede ser menor de 18")
            ->notEmpty('edad','La edad no puede estar vacia');

        return $validator;
    }

    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['usuario'],"Este usuario ya existe"));

        return $rules;
    }


    function afterDelete(Event $event, EntityInterface $entity, ArrayObject $options){
        $this->Favoritos->deleteAll([
            'codigousuariofavorito' => $entity->get("codigousuario")
        ]);
    }

}
