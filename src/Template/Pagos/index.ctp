<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Pago[]|\Cake\Collection\CollectionInterface $pagos
 */
?>
<div class="usuarios index large-9 medium-8 columns content mt-5">
    <h3><?= __('Pagos') ?></h3>
    <table class="table">
        <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('codigopago',"Codigo") ?></th>
            <th scope="col"><?= $this->Paginator->sort('importe') ?></th>
            <th scope="col"><?= $this->Paginator->sort('fecha') ?></th>
            <th scope="col" class="actions"><?= __('Acciones') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($pagos as $pago): ?>
            <tr>
                <td><?= $this->Number->format($pago->codigopago) ?></td>
                <td><?= h($pago->importe) ?></td>
                <td><?= h($pago->fecha) ?></td>
                <td class="actions">
                    <?php echo $this->Html->link(
                        '<i class="fa-edit fa fa-1"></i>',
                        array(
                            'action' => 'edit',
                            $pago->codigopago,
                        ),array('escape' => false,'class'=>'btn btn-xs btn-info btn-flat','title'=>__("Edit"))
                    );?>
                    <?= $this->Form->postLink(
                        '<i class="fa-trash fa fa-1"></i>',
                        ['action' => 'delete',
                            $pago->codigopago,
                        ],['escape' => false,'class'=>'btn btn-xs btn-danger btn-flat','title'=>__("Delete"),
                        'confirm' => __('Seguro deseas eliminar el pago # {0}?', $pago->codigopago)
                    ])?>

<!--                    --><?php //echo $this->Html->link(
//                        '<i class="fa-star fa fa-1"></i>',
//                        array(
//                            'action' => 'add-favorito',
//                            $pago->codigopago
//                        ),array('escape' => false,'class'=>'btn btn-xs btn-success btn-flat','title'=>__("Favorite"))
//                    );?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('Prev')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Sig') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Pagina {{page}} de {{pages}}, mostrando {{current}} record(s) de {{count}} en total')]) ?></p>
    </div>
    <div class="text-center">
        <?= $this->Html->link(__('Crear Pago'), ['action' => 'add'],["class" => "btn btn-success"]) ?>
    </div>
</div>
