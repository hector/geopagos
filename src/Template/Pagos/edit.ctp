<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Usuario $usuario
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Lista de pagos'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="usuarios form large-9 medium-8 columns content">
    <?= $this->Form->create($pago,array(
        'novalidate' => true,
        'class' => '',
        'role' => 'form',
        'type' => 'file'
    )); ?>
    <fieldset>
        <legend><?= __('Editando pago'); ?></legend>

        <div class="form-group">
            <label><?=__("Importe");?></label>
            <?php echo $this->Form->control('Pagos.importe', ['class' => 'form-control','label' => false]); ?>
        </div>
        <div class="form-group">
            <label><?=__("fecha");?></label>
            <?php echo $this->Form->control('Pagos.fecha', ['class' => 'form-control','type'=>'date','label' => false]); ?>
        </div>
    </fieldset>
    <?= $this->Form->button(__('Submit'),["class" => "btn"]) ?>
    <?= $this->Form->end() ?>

    <div class="mt-5">
        <h3>Este pago es del usuario</h3>
        <table class="table">
                <tr>
                    <td>
                        <?php
                        if($pago->usuariospago){
                            echo $pago->usuariospago->usuario->usuario;
                        }else{
                            echo "este pago no tiene un usuario";
                        }?>
                    </td>
                </tr>
        </table>
    </div>
</div>

