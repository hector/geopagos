<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Usuario $usuario
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Lista de usuarios'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="usuarios form large-9 medium-8 columns content">
    <?= $this->Form->create($usuario,array(
        'novalidate' => true,
        'class' => '',
        'role' => 'form',
        'type' => 'file'
    )); ?>
    <fieldset>
        <legend><?= __('Nuevo Usuario') ?></legend>

        <div class="form-group">
            <label><?=__("Usuario");?></label>
            <?php echo $this->Form->control('Usuarios.usuario', ['class' => 'form-control','label' => false]); ?>
        </div>
        <div class="form-group">
            <label><?=__("Clave");?></label>
            <?php echo $this->Form->control('Usuarios.clave', ['type' => 'password','class' => 'form-control','label' => false]); ?>
        </div>
        <div class="form-group">
            <label><?=__("Edad");?></label>
            <?php echo $this->Form->control('Usuarios.edad', ['class' => 'form-control','label' => false]); ?>
        </div>
    </fieldset>
    <?= $this->Form->button(__('Submit'),["class" => "btn"]) ?>
    <?= $this->Form->end() ?>
</div>
