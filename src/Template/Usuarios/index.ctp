<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Usuario[]|\Cake\Collection\CollectionInterface $usuarios
 */
?>
<div class="usuarios index large-9 medium-8 columns content mt-5">
    <h3><?= __('Usuarios') ?></h3>
    <table class="table">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('codigousuario',"Codigo") ?></th>
                <th scope="col"><?= $this->Paginator->sort('usuario') ?></th>
                <th scope="col"><?= $this->Paginator->sort('edad') ?></th>
                <th scope="col" class="actions"><?= __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($usuarios as $usuario): ?>
            <tr>
                <td><?= $this->Number->format($usuario->codigousuario) ?></td>
                <td><?= h($usuario->usuario) ?></td>
                <td><?= $this->Number->format($usuario->edad) ?></td>
                <td class="actions">
                    <?php echo $this->Html->link(
                        '<i class="fa-edit fa fa-1"></i>',
                        array(
                            'action' => 'edit',
                            $usuario->codigousuario,
                        ),array('escape' => false,'class'=>'btn btn-xs btn-info btn-flat','title'=>__("Edit"))
                    );?>
                    <?= $this->Form->postLink(
                        '<i class="fa-trash fa fa-1"></i>',
                        ['action' => 'delete',
                          $usuario->codigousuario,
                        ],['escape' => false,'class'=>'btn btn-xs btn-danger btn-flat','title'=>__("Delete"),
                        'confirm' => __('Are you sure you want to delete # {0}?', $usuario->codigousuario)
                        ])?>

                    <?php echo $this->Html->link(
                        '<i class="fa-star fa fa-1"></i>',
                        array(
                            'action' => 'add-favorito',
                            $usuario->codigousuario,
                        ),array('escape' => false,'class'=>'btn btn-xs btn-success btn-flat','title'=>__("Favorite"))
                    );?>

                    <?php echo $this->Html->link(
                        '<i class="fa-money fa fa-1"></i>',
                        array(
                            'action' => 'add-pago',
                            $usuario->codigousuario,
                        ),array('escape' => false,'class'=>'btn btn-xs btn-primary btn-flat','title'=>__("Pago"))
                    );?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('Prev')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Sig') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Pagina {{page}} de {{pages}}, mostrando {{current}} record(s) de {{count}} en total')]) ?></p>
    </div>
    <div class="text-center">
        <?= $this->Html->link(__('Crear Usuario'), ['action' => 'add'],["class" => "btn btn-success"]) ?>
    </div>
</div>
