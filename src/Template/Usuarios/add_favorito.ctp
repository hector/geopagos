<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Usuario $usuario
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Lista de usuarios'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="usuarios form large-9 medium-8 columns content">
    <?= $this->Form->create($favorito,array(
        'novalidate' => true,
        'class' => '',
        'role' => 'form',
        'type' => 'file'
    )); ?>
    <fieldset>
        <legend><?= __('Agregar favorito al usuario "'.$user->get("usuario").'"') ?></legend>

        <div class="form-group">
            <label><?=__("Usuario");?></label>
            <?php echo $this->Form->control("usuario", ['type'=> 'text','class' => 'form-control','label' => false,'disabled'=>'disabled','value' => $user->get("usuario") ]); ?>
            <?php echo $this->Form->control("Favoritos.codigousuario", ['type'=> 'hidden','class' => 'form-control','label' => false,'value' => $user->get("codigousuario")]); ?>

        </div>
<!--        <div class="form-group">-->
<!--            <label>--><?//=__("Usuario");?><!--</label>-->
<!--            --><?php //echo $this->Form->control("Favoritos.codigousuariofavorito", ['type'=> 'text','class' => 'form-control','label' => false]); ?>
<!---->
<!--        </div>-->
        <div class="form-group">
            <label><?=__("Seleccione nuevo favorito");?></label>
            <?php echo $this->Form->control('Favoritos.codigousuariofavorito',
                [ 'type' => 'select',
                    'options' =>[0 => "- Seleccionar"] + $usuarios,
                    'class' => 'form-control', 'label' => false
                ]); ?>
        </div>
    </fieldset>
    <?= $this->Form->button(__('Submit'),["class" => "btn"]) ?>
    <?= $this->Form->end() ?>

    <div class="mt-5">
        <h3>Favoritos</h3>
        <table class="table">
            <?php foreach ($user["favoritos"] as $favorito) {?>
            <tr>
                <td><?=$usuarios[$favorito->get("codigousuariofavorito")]?></td>
                <td>
                    <?= $this->Form->postLink(__('Eliminar'), ['action' => 'remove_favorite', $user->get("codigousuario"),$favorito->get("codigousuariofavorito")], ['confirm' => __('seguro desea eliminar este favorito?')]) ?>
            </tr>
            <?php }?>
        </table>
    </div>
</div>
