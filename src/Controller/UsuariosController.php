<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Usuarios Controller
 *
 * @property \App\Model\Table\UsuariosTable $Usuarios
 *
 * @method \App\Model\Entity\Usuario[] paginate($object = null, array $settings = [])
 */
class UsuariosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

        $query = $this->Usuarios->find()->contain(['Usuariospagos'])->all()->toArray();

        $usuarios = $this->paginate($this->Usuarios);

        $this->set(compact('usuarios'));
        $this->set('_serialize', ['usuarios']);
    }

    /**
     * View method
     *
     * @param string|null $id Usuario id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $usuario = $this->Usuarios->get($id, [
            'contain' => ['Favoritos']
        ]);

        $this->set('usuario', $usuario);
        $this->set('_serialize', ['usuario']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $usuario = $this->Usuarios->newEntity();
        if ($this->request->is('post')) {
            $usuario = $this->Usuarios->patchEntity($usuario, $this->request->getData());
            if ($this->Usuarios->save($usuario)) {
                $this->Flash->success(__('El usuario ha sido creado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se pudo crear el usuario.'));
        }
        $this->set(compact('usuario'));
        $this->set('_serialize', ['usuario']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Usuario id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $usuario = $this->Usuarios->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            if(empty($data["Usuarios"]['clave'])){
                unset($data["Usuarios"]['clave']);
            }
            $usuario = $this->Usuarios->patchEntity($usuario, $data);
            if ($this->Usuarios->save($usuario)) {
                $this->Flash->success(__('El usuario ha sido actualizado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se pudo actualizar el usuario.'));
        }
        $this->set(compact('usuario'));
        $this->set('_serialize', ['usuario']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Usuario id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $usuario = $this->Usuarios->get($id);
        if ($this->Usuarios->delete($usuario)) {
            $this->Flash->success(__('Se ha eliminado el usuario'));
        } else {
            $this->Flash->error(__('No se pudo eliminar el usuario'));
        }

        return $this->redirect(['action' => 'index']);
    }

    function addFavorito($user_id){
        $user = $this->Usuarios->get($user_id, [
            'contain' => ['Favoritos']
        ]);

        $favorito = $this->Usuarios->Favoritos->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $favorito = $this->Usuarios->Favoritos->patchEntity($favorito, $this->request->getData(),['associated' => ['Usuarios']]);
            if ($this->Usuarios->Favoritos->save($favorito)) {
                $this->Flash->success(__('El favorito se ha sido agregado.'));
                return $this->redirect(['action' => 'addFavorito',$user_id]);
            }
            $this->Flash->error(__('No se pudo agregar el favorito.'));
        }

        $usuarios = $this->Usuarios->find('list', ['limit' => 200,
            'keyField' => 'codigousuario',
            'valueField' => 'usuario'
        ])->where(["Usuarios.codigousuario <>" => $user_id])->toArray();

        $this->set(compact('usuarios','user','favorito'));
        $this->set('_serialize', ['usuarios','favorito']);
    }

    function removeFavorite($codigousuario,$codigousuariofavorito){
        $this->request->allowMethod(['post', 'delete']);
        if($this->Usuarios->Favoritos->deleteAll([
            'codigousuario' => $codigousuario,
            'codigousuariofavorito' => $codigousuariofavorito
        ])){
            $this->Flash->success(__('El favorito se ha removido.'));
            return $this->redirect(['action' => 'addFavorito',$codigousuario]);
        }else{
            $this->Flash->error(__('no se pudo quitar el favorito'));
            return $this->redirect(['action' => 'addFavorito',$codigousuario]);
        }
    }

    public function addPago($user_id)
    {
        $Pagos = TableRegistry::get("Pagos");
        $user = $this->Usuarios->get($user_id, [
            'contain' => ['Usuariospagos']
        ]);
        $pago = $Pagos->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $pago = $Pagos->patchEntity($pago, $data);

            if ($Pagos->save($pago)) {
                $usuariospagos = $Pagos->Usuariospagos->newEntity();
                $usuariospagos = $Pagos->Usuariospagos->patchEntity($usuariospagos,["codigousuario" => $user_id,"codigopago" => $pago->codigopago]);
                $result = $Pagos->Usuariospagos->save($usuariospagos);
                if($result){
                    $this->Flash->success(__('El pago se ha sido agregado.'));
                    return $this->redirect(['action' => 'index']);
                }

            }
            $this->Flash->error(__('No se pudo agregar el pago.'));
        }

        $this->set(compact('user','pago'));
        $this->set('_serialize', ['user','pago']);
    }

}
